package de.daJonny.IBANCalculator;

import java.math.BigInteger;

/**
 * This class contains a static which calculates our IBAN from the German BLZ and account number
 * 
 * @author Jonathan Lukas
 * @version 1.0
 * @since 15.05.2017
 */
public class IBANRechner {

    /**
     * this static method returns your IBAN as a String
     * 
     * @param blz
     * @param ktoNr
     * @return
     */
    public static String getIBAN(int blz, int ktoNr) {
        return pruefrechner(blz, ktoNr) + String.format("%08d", blz) + String.format("%010d", ktoNr);
    }

    
    /**
     * this static method calculates your IBAN check
     * 
     * @param blz
     * @param ktoNr
     * @return
     */
    private static String pruefrechner(int blz, int ktoNr) {
        String pruefwert = "DE";
        BigInteger summe = new BigInteger(new String(blz + String.format("%010d", ktoNr) + "131400"));
        BigInteger faktor = new BigInteger("97");
        long div = summe.remainder(faktor).longValue();
        int pruef = (int) (98 - div);
        pruefwert += String.format("%02d", pruef);
        return pruefwert;
    }
}
